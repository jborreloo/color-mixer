#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(16, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

const int redPin = 2;
const int greenPin = 4;
const int bluePin = 8;
const int ledPin = 13;

int redState = 0;
int redCount = 0;
int redCountDiv;
int r = 0;

int greenState = 0;
int greenCount = 0;
int greenCountDiv;
int g = 0;

int blueState = 0;
int blueCount = 0;
int blueCountDiv;
int b = 0;

void setup(){
  pinMode(redPin, INPUT);
  pinMode(greenPin, INPUT);
  pinMode(bluePin, INPUT);
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);

  strip.begin();
  strip.show();
}

void loop(){
  redState = digitalRead(redPin);
  greenState = digitalRead(greenPin);
  blueState = digitalRead(bluePin);

  if (redState == HIGH){
    digitalWrite(ledPin, HIGH);
    redCount ++;
    delay(250);
  }
  else{
    digitalWrite(ledPin, LOW);
  }
  if (greenState == HIGH){
    digitalWrite(ledPin, HIGH);
    greenCount ++;
    delay(250);
  }
  else{
    digitalWrite(ledPin, LOW);
  }
  if (blueState == HIGH){
    digitalWrite(ledPin, HIGH);
    blueCount ++;
    delay(250);
  }
  else{
    digitalWrite(ledPin, LOW);
  }
  
  redCountDiv = redCount % 6;
  greenCountDiv = greenCount % 6;
  blueCountDiv = blueCount % 6;
  
  Serial.println("redCount");
  Serial.println(redCount);
  Serial.println("redCountDiv");
  Serial.println(redCountDiv);
  Serial.println("greenCount");
  Serial.println(greenCount);
  Serial.println("greenCountDiv");
  Serial.println(greenCountDiv);
  Serial.println("blueCount");
  Serial.println(blueCount);
  Serial.println("blueCountDiv");
  Serial.println(blueCountDiv);
  
  switch(redCountDiv){
    case 1:
      r = 50;
      break;
    case 2:
      r = 100;
      break;
    case 3:
      r = 150;
      break;
    case 4:
      r = 200;
      break;
    case 5:
      r = 250;
      break;
    default:
      r = 0;
    break;
  }
  switch(greenCountDiv){
    case 1:
      g = 50;
      break;
    case 2:
      g = 100;
      break;
    case 3:
      g = 150;
      break;
    case 4:
      g = 200;
      break;
    case 5:
      g = 250;
      break;
    default:
      g = 0;
    break;
  }
  switch(blueCountDiv){
    case 1:
      b = 50;
      break;
    case 2:
      b = 100;
      break;
    case 3:
      b = 150;
      break;
    case 4:
      b = 200;
      break;
    case 5:
      b = 250;
      break;
    default:
      b = 0;
    break;
  }
  
  Serial.println("r");
  Serial.println(r);
  Serial.println("g");
  Serial.println(g);
  Serial.println("b");
  Serial.println(b);
  
  for (int i=0; i<16; i++){
    strip.setPixelColor(i, r, g, b);
  }
  strip.show();
  //delay(250);
}